package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.entity.EmpresaLocal;

public interface EmpresaLocalDao extends CrudRepository<EmpresaLocal, Long> {

	@Query("select e from EmpresaLocal e where e.nombre like %?1%")
	public List<EmpresaLocal> findByNombre(String term);
	@Query("select e from EmpresaLocal e where e.nombre=?1")
	public EmpresaLocal solonombre(String nombre);
	
}
