package com.bolsadeideas.springboot.app.models.service;

import java.util.Date;
import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Asistencia;
import com.bolsadeideas.springboot.app.models.entity.Empleado;

public interface AsistenciaService {
	
	public void saveAsistencia(Asistencia asistencia);
	
	public Asistencia findAsistenciaByID(Long id);
	
	public void deleteAsistencia(Asistencia asistencia);
	
	public List<Asistencia> findAllAsistencia();
	
	public List<Asistencia> findByFecha(String term);
	
	public List<Asistencia> findByFechaEspecifica(String term);
	
	public List<Asistencia> findByNombreFecha(String fecha,String fecha2,Long id);
	
	public List<Asistencia> findByrangoFecha(String fecha,String fecha2);
}
