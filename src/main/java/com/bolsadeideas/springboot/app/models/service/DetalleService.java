package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.Factura;

public interface DetalleService {

	public void eliminarFactura(Factura factura);
	
   public Factura factura(Long id);
   
   public List<Factura> todos();
   
   public List<Factura> todosTermino(String term);
}
