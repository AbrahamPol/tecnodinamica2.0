package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.Producto;

public interface ConcluirService {

	public List<Cotizacion> todos ();
	
	public Cotizacion findOne(Long id);
	
	public List<Producto> findByNombre(String term);
	
	public Producto findByID(Long id);
	
	public void saveFactura(Factura factura);
}
