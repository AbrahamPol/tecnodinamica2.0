package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.CotizacionDao;
import com.bolsadeideas.springboot.app.models.dao.FacturaDao;
import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.Factura;

@Service
public class DetalleServiceImpl implements DetalleService {

	@Autowired
	FacturaDao facturadao;

	@Autowired
	CotizacionDao cotizacionDao;

	@Override
	public Factura factura(Long id) {
		return facturadao.facturacompleta(id);
	}

	@Override
	public List<Factura> todos() {
		return facturadao.todos();
	}

	@Override
	public void eliminarFactura(Factura factura) {
		Cotizacion cotizacion = cotizacionDao.findOne(factura.getCotizacion().getId());
		facturadao.delete(factura.getId());
		cotizacionDao.delete(cotizacion);

	}

	@Override
	public List<Factura> todosTermino(String term) {
		return facturadao.todosTermino(term);
	}

}
