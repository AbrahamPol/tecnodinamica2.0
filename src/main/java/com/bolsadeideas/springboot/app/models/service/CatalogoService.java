package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;

public interface CatalogoService {

	public void guardarempresa(CatalogoEmpresa catalogo);
	
	public List<CatalogoEmpresa> todosCatalogo();
	
	public void eliminarEmpresaCatalogo(CatalogoEmpresa catalogo);
	
	public void editarEmpresaCatalogo(Long id);
	
	public CatalogoEmpresa findById(Long id);
	
}
