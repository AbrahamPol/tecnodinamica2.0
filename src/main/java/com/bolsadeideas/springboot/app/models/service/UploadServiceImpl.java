package com.bolsadeideas.springboot.app.models.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
@Service
public class UploadServiceImpl implements UploadFileService {

	public final static String UPLOADS_FOLDER="uploads";
	
	@Override
	public void init() throws IOException{
		Files.createDirectory(Paths.get(UPLOADS_FOLDER));

	}

	@Override
	public void delete() {
		FileSystemUtils.deleteRecursively(Paths.get(UPLOADS_FOLDER).toFile());
	}

}
