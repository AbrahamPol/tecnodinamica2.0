package com.bolsadeideas.springboot.app.models.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.AsistenciaDao;
import com.bolsadeideas.springboot.app.models.entity.Asistencia;

@Service
public class AsistenciaServiceImpl implements AsistenciaService {

	@Autowired
	AsistenciaDao asistenciaDao;

	@Override
	public void saveAsistencia(Asistencia asistencia) {
		asistenciaDao.save(asistencia);
	}

	@Override
	public Asistencia findAsistenciaByID(Long id) {
		// TODO Auto-generated method stub
		return asistenciaDao.findOne(id);
	}

	@Override
	public List<Asistencia> findAllAsistencia() {
		// TODO Auto-generated method stub
		return asistenciaDao.findByFechaHoy();
	}

	@Override
	public void deleteAsistencia(Asistencia asistencia) {
		asistenciaDao.delete(asistencia);
	}

	@Override
	public List<Asistencia> findByFecha(String term) {
		return asistenciaDao.findByFecha(term);
	}

	@Override
	public List<Asistencia> findByFechaEspecifica(String term) {
		return asistenciaDao.findByFechaEspecifica(term);
	}

	@Override
	public List<Asistencia> findByNombreFecha(String fecha, String fecha2, Long id) {
		return asistenciaDao.findByNombreFecha(fecha, fecha2, id);
	}

	@Override
	public List<Asistencia> findByrangoFecha(String fecha, String fecha2) {
		return asistenciaDao.findByrangoFecha(fecha, fecha2);
	}

}
