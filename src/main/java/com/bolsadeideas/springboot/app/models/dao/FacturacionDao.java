package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Facturacion;
import com.bolsadeideas.springboot.app.models.entity.Producto;

public interface FacturacionDao extends CrudRepository<Facturacion, Long>{
	
	@Query("select f from Facturacion f where f.pedido like %?1%")
	public List<Facturacion> findByPedido(String term);
}
