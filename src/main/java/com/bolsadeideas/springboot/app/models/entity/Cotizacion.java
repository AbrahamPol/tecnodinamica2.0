package com.bolsadeideas.springboot.app.models.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "Cotizacion")
public class Cotizacion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique=true)
	@NotEmpty
	private String numero_cotizacion;
	@Column(unique=true)
	@NotEmpty
	private String referencia;
	@NotEmpty
	private String cargo_nombre;
	@NotEmpty
	private String departamento;
	@Email
	@NotEmpty
	private String correo;
	@NotEmpty
	private String asunto;
	
	private Date fecha;
	
	@PrePersist
	public void prePersist() {
		fecha= new Date();
	}
	@OneToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="empresa_local_id")
	private EmpresaLocal empresa_local;
	
	@OneToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="empresa_catalogo_id")
	private CatalogoEmpresa empresa_catalogo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNumero_cotizacion() {
		return numero_cotizacion;
	}
	public void setNumero_cotizacion(String numero_cotizacion) {
		this.numero_cotizacion = numero_cotizacion;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getCargo_nombre() {
		return cargo_nombre;
	}
	public void setCargo_nombre(String cargo_nombre) {
		this.cargo_nombre = cargo_nombre;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}


	public EmpresaLocal getEmpresa_local() {
		return empresa_local;
	}
	public void setEmpresa_local(EmpresaLocal empresa_local) {
		this.empresa_local = empresa_local;
	}
	public CatalogoEmpresa getEmpresa_catalogo() {
		return empresa_catalogo;
	}
	public void setEmpresa_catalogo(CatalogoEmpresa empresa_catalogo) {
		this.empresa_catalogo = empresa_catalogo;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	@Override
	public String toString() {
		return "Cotizacion [id=" + id + ", numero_cotizacion=" + numero_cotizacion + ", referencia=" + referencia
				+ ", cargo_nombre=" + cargo_nombre + ", departamento=" + departamento + ", correo=" + correo
				+ ", asunto=" + asunto + ", fecha=" + fecha + ", empresa_local=" + empresa_local + ", empresa_catalogo="
				+ empresa_catalogo + "]";
	}

}