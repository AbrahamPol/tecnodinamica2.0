package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Factura;

public interface FacturaDao extends CrudRepository<Factura, Long>{

	@Query(value = "select * from factura inner join cotizacion on cotizacion.id=factura.cotizacion_id  where factura.id=?1" ,nativeQuery = true)
	public Factura facturacompleta(Long id);
	
	@Query(value = "select * from factura inner join cotizacion on cotizacion.id=factura.cotizacion_id  " ,nativeQuery = true)
	public List<Factura> todos();
	
	@Query(value = "select * from factura inner join cotizacion on cotizacion.id=factura.cotizacion_id  where numero_cotizacion  like %?1%" ,nativeQuery = true)
	public List<Factura> todosTermino(String term);
	
	
}
