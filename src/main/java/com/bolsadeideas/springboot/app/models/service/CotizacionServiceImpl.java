package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.CatalogoEmpresaDao;
import com.bolsadeideas.springboot.app.models.dao.CotizacionDao;
import com.bolsadeideas.springboot.app.models.dao.EmpresaLocalDao;
import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.EmpresaLocal;

@Service
public class CotizacionServiceImpl implements CotizacionService {
	@Autowired
	CotizacionDao cotizaciondao;
	
	@Autowired
	EmpresaLocalDao empresalocaldao;
	
	@Autowired
	CatalogoEmpresaDao empresacatalogodao;
	

	@Override
	public void guardar(Cotizacion cotizacion) {
		cotizaciondao.save(cotizacion);

	}

	@Override
	public Cotizacion cotizacion(Long id) {
		return cotizaciondao.findOne(id);

	}

	@Override
	public List<Cotizacion> todos() {
		return cotizaciondao.concluidas();
	}

	@Override
	public List<EmpresaLocal> findLocalName(String term) {
		return empresalocaldao.findByNombre(term);
	}

	@Override
	public List<CatalogoEmpresa> findCatalogoName(String term) {
		return empresacatalogodao.findByNombre(term);
	}

	@Override
	public EmpresaLocal findLocalId(Long id) {
	return	empresalocaldao.findOne(id);
	}

	@Override
	public CatalogoEmpresa findCatalogoId(Long id) {
		
		return empresacatalogodao.findOne(id);
	}

	@Override
	public EmpresaLocal solonombre(String nombre) {
		return empresalocaldao.solonombre(nombre);
	}

	@Override
	public CatalogoEmpresa solonombre2(String nombre) {
		return empresacatalogodao.solonombre(nombre);
	}




}
