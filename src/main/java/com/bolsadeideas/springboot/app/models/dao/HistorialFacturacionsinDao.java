package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacionsin;

public interface HistorialFacturacionsinDao extends CrudRepository<HistorialFacturacionsin, Long> {

	@Query(value = "select * from historial_facturacionsin hf inner join facturacionsin f on f.id=hf.facturacion_id where hf.facturacion_id=?1", nativeQuery = true)
	public HistorialFacturacionsin findOne(Long id);

//	@SQLUpdate(sql="insert into historial_facturacion values(null,?1,?2,?3,?4,?5)",callable=true)
//	public void actualizarPago(float abonado,String archivo,String pago,String Porcentaje, Long id);
}
