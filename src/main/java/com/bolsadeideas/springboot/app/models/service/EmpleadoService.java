package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Empleado;

public interface EmpleadoService {

	public void saveEmpleado(Empleado empleado);
	
	public Empleado findEmpleadoByID(Long id);
	
	public void deleteEmpleado(Empleado empleado);
	
	public List<Empleado> findAllEmpleados();
	
	public List<Empleado> findByNombre(String term);
	
}
