package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.EmpresaLocal;

public interface CotizacionService {

	public void guardar(Cotizacion cotizacion);
	
	public Cotizacion cotizacion(Long id);
	
	public List<Cotizacion> todos ();
	
	public List<EmpresaLocal> findLocalName(String term);
	
	public List<CatalogoEmpresa> findCatalogoName(String term);
	
	public EmpresaLocal findLocalId(Long id);
	
	public CatalogoEmpresa findCatalogoId(Long id);
	
	public EmpresaLocal solonombre(String nombre);
	
	public CatalogoEmpresa solonombre2(String nombre);
	
}
