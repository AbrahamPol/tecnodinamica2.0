package com.bolsadeideas.springboot.app.models.service;

import java.io.IOException;

public interface UploadFileService {

	public void init() throws IOException;
	
	public void delete();
}
