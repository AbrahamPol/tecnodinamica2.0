package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Asistencia;

public interface AsistenciaDao extends CrudRepository<Asistencia, Long>{
	
	
	@Query(value="select * from asistencia a inner join empleado e on e.id=a.empleado_id where a.fecha like %?1%",nativeQuery=true)
	public List<Asistencia> findByFecha(String term);

	@Query(value="select * from asistencia a inner join empleado e on e.id=a.empleado_id where a.fecha=?1",nativeQuery=true)
	public List<Asistencia> findByFechaEspecifica(String term);
	
	@Query(value="select * from asistencia a inner join empleado e on e.id=a.empleado_id where a.fecha=curdate();",nativeQuery=true)
	public List<Asistencia> findByFechaHoy();
	
	@Query(value="select * from asistencia a inner join empleado e on e.id=a.empleado_id where a.fecha BETWEEN ?1 AND ?2 and a.empleado_id=?3",nativeQuery=true)
	public List<Asistencia> findByNombreFecha(String fecha,String fecha2,Long id);
	
	@Query(value="select * from asistencia a inner join empleado e on e.id=a.empleado_id where a.fecha BETWEEN ?1 AND ?2 ",nativeQuery=true)
	public List<Asistencia> findByrangoFecha(String fecha,String fecha2);
	
}
