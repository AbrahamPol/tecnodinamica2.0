package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.CatalogoEmpresaDao;
import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;

@Service
public class CatalogoServiceImpl implements CatalogoService {

	@Autowired
	CatalogoEmpresaDao catalogodao;

	@Override
	public void guardarempresa(CatalogoEmpresa catalogo) {
		catalogodao.save(catalogo);

	}

	@Override
	public List<CatalogoEmpresa> todosCatalogo() {
		return (List<CatalogoEmpresa>) catalogodao.findAllTrue();
	}

	@Override
	public void editarEmpresaCatalogo(Long id) {
		// catalogodao.
	}

	@Override
	public CatalogoEmpresa findById(Long id) {
		return catalogodao.findOne(id);
	}

	@Override
	public void eliminarEmpresaCatalogo(CatalogoEmpresa catalogo) {
//	catalogo.setStatus(false);
//	catalogodao.save(catalogo);
	}

}
