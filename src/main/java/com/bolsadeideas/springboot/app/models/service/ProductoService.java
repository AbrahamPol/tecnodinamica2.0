package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Producto;

public interface ProductoService {

	
	public List<Producto> todos();
	
	public void guardarActualizar(Producto producto);
	
	public Producto finById(Long id);
	
	public void deleteByID(Long id);
	
}
