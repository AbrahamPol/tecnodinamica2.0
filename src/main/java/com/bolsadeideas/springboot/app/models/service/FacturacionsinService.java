package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.FacturacionSin;
import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacionsin;

public interface FacturacionsinService {

	//guardar
		public void save(FacturacionSin facturacion);
		//buscar todos
		public List<FacturacionSin> findAll();
		
		//buscar por id
		public FacturacionSin findById(Long id);
		//actualizar
		public void update(FacturacionSin facturacion);
		//eliminar
		public void delete(Long id);	
		
		/*
		 * Metodos para guardar el historial de pago
		 */
		public void saveHistorial(HistorialFacturacionsin historial);	
		public void updateHistorial(HistorialFacturacionsin historial);
		public HistorialFacturacionsin findOne (Long id);	
		public List<HistorialFacturacionsin> findAllHistorial();
		public void actualizarPago(float abonado,String archivo,String pago,String Porcentaje,Long id);
		public List<FacturacionSin> findByProceso(String term);
		public List<FacturacionSin> findByProceso2(String term);
		public FacturacionSin findByPedido(String term);
}
