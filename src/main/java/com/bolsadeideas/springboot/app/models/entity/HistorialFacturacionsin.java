package com.bolsadeideas.springboot.app.models.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "HistorialFacturacionsin")
public class HistorialFacturacionsin {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "facturacion_id")
	private FacturacionSin facturacion;

	
	private String porcentaje;

	private float abonado;

	private String no_pago;

	private String archivo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public FacturacionSin getFacturacion() {
		return facturacion;
	}

	public void setFacturacion(FacturacionSin facturacion) {
		this.facturacion = facturacion;
	}

	public String getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}

	public float getAbonado() {
		return abonado;
	}

	public void setAbonado(float abonado) {
		this.abonado = abonado;
	}

	public String getNo_pago() {
		return no_pago;
	}

	public void setNo_pago(String no_pago) {
		this.no_pago = no_pago;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	@Override
	public String toString() {
		return "HistorialFacturacionsin [id=" + id + ", facturacion=" + facturacion + ", porcentaje=" + porcentaje
				+ ", abonado=" + abonado + ", no_pago=" + no_pago + ", archivo=" + archivo + "]";
	}
	
	
	
	
	
}
