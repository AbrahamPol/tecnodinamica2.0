package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.hibernate.annotations.SQLUpdate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.HistorialFacturacion;

public interface HistorialFacturacionDao extends CrudRepository<HistorialFacturacion, Long> {

	@Query(value = "select * from historial_facturacion hf inner join facturacion f on f.id=hf.facturacion_id where hf.facturacion_id=?1", nativeQuery = true)
	public HistorialFacturacion findOne(Long id);

//	@SQLUpdate(sql="insert into historial_facturacion values(null,?1,?2,?3,?4,?5)",callable=true)
//	public void actualizarPago(float abonado,String archivo,String pago,String Porcentaje, Long id);
}
