package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Cotizacion;

public interface CotizacionDao extends CrudRepository<Cotizacion, Long> {

	@Query(value = "select * from cotizacion inner join factura on factura.cotizacion_id=cotizacion.id " ,nativeQuery = true)
	public List<Cotizacion> concluidas();
	
	@Query(value = "select * from cotizacion c\r\n" + 
			"where NOT EXISTS(SELECT null from factura f where c.id=f.cotizacion_id) " ,nativeQuery = true)
	public List<Cotizacion> noconcluidas();
	
	
	
}
