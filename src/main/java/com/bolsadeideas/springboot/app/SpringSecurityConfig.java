package com.bolsadeideas.springboot.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableGlobalMethodSecurity(securedEnabled=true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private LoginSuccessHandler successHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().
		antMatchers( "/css/**", "/js/**", "/images/**").
		permitAll().
		anyRequest()
				.authenticated().
				and().
				formLogin().
				successHandler(successHandler).
				loginPage("/login").permitAll().and()
				.logout().
				permitAll().and()
				.exceptionHandling().accessDeniedPage("/error_403");

	}

	@Autowired
	public void configurerGlobal(AuthenticationManagerBuilder build) throws Exception {
		build.inMemoryAuthentication().
		withUser("user").password("user").roles("USER").and().
		withUser("admin").password("admin").roles("USER", "ADMIN","INVITADO").and().
		withUser("inventario").password("tecnodinamica").roles("INVITADO");
	}
}
