package com.bolsadeideas.springboot.app;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bolsadeideas.springboot.app.models.dao.CatalogoEmpresaDao;
import com.bolsadeideas.springboot.app.models.dao.EmpresaLocalDao;
import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.entity.EmpresaLocal;

@Component
public class DummyInjection {

	@Autowired
	CatalogoEmpresaDao catalogodao;
	@Autowired
	EmpresaLocalDao empresalocaldao;
	
	@PostConstruct
	public boolean createRole(){
		EmpresaLocal empresaModel= new EmpresaLocal(1L,"TECNO DINAMICA DCYA SA DE CV","Calle Laurel No. 30, Col. Villa del Real Loma Alta, Tecámac, Edo. de México, C.P. 55770","5522210571","http://www.tecnodinamica.com.mx","Tecnodinamica.jpg");
		empresalocaldao.save(empresaModel);
		
		//save the first register
		CatalogoEmpresa catalogoModel1= new CatalogoEmpresa(1L,"Hermos S.A de C.V","Carr.Panamericana Celaya-Salamanca No.114  38020 Celaya, Guanajuato.","(461) 618-7300","http://www.hermos.com.mx/","hermos.png",true);
		catalogodao.save(catalogoModel1);
		
		//sve the second register
		CatalogoEmpresa catalogoModel2= new CatalogoEmpresa(2L,"Risoul","MEXICO  Av. Tochtli No. 323 - 3 Col. Fracc. Ind. San Antonio    Delegacion Azcapotzalco    Ciudad de México. C.P.02760   entre Av. Tezozomoc y calle Centeotl. ","63-89-10-70","https://www.risoul.com.mx/","risoul.png",true);
		catalogodao.save(catalogoModel2);
		
		//save the 3 register
		CatalogoEmpresa catalogoModel3= new CatalogoEmpresa(3L,"La Casa del control y El Gabinete","Pescaditos No. 3, Col. Centro,Del. Cuauhtémoc, CDMX","5510 3349","https://www.lacasadelcontrol.com.mx/","lacasa.png",true);
		catalogodao.save(catalogoModel3);
		
		//save the 4 register
		CatalogoEmpresa catalogoModel4= new CatalogoEmpresa(4L,"DIMEINT","Manzanillo # 807,Col. San Jerónimo Chicahualco,Metepec, Edo. de México, C.P. 52170. Toluca","722 275 0863","https://dimeint.mx/","DIMEINT.png",true);
		catalogodao.save(catalogoModel4);
		
		//save the 5 register
		CatalogoEmpresa catalogoModel5= new CatalogoEmpresa(5L,"MEGA CONTROL Y SUMINISTROS","Convento de San Bernardo 5 Jardines de Santa Mónica 54050 Tlalnepantla, Estado De Mexico Mexico","(55) 6628 9413","https://megaenlinea.com/","mega.png",true);
		catalogodao.save(catalogoModel5);
		
		//save the 6 register
		CatalogoEmpresa catalogoModel6= new CatalogoEmpresa(6L,"Electrotecnia","Carretera a Eldorado km 2.5 Campo el Diez. C.P. 80300","(55) 2650-3797","https://www.electrotecnicareal.com/","electrotecnia.png",true);
		catalogodao.save(catalogoModel6);
		
		//save the 7 register
		CatalogoEmpresa catalogoModel7= new CatalogoEmpresa(7L,"ProSoft Technology","Boulevard Via Atlixcayotl 5208 Torre JV1 piso 14 B | San Andrés Cholula, Puebla 72820, México","52 222 264 18 14","https://mx.prosoft-technology.com/","prosofttechnology.png",true);
	    catalogodao.save(catalogoModel7);
	    
	    //save the 8 register
	    CatalogoEmpresa catalogoModel8= new CatalogoEmpresa(8L,"ECI","COL. CENTRO DEL. CUAUHTEMOC CDMX","5521 7626","https://ecisacv.com/empresa/acerca-de/","eci.jpeg",true);
		catalogodao.save(catalogoModel8);
		
		//save the 9 register
		CatalogoEmpresa catalogoModel9= new CatalogoEmpresa(9L,"Ticsa","Carr.Panamericana Celaya-Salamanca No.114  38020   Celaya, Guanajuato Mexico","52-553098 5600","http://www.ticsa.com.mx/epm.html","TICSA.png",true);
		catalogodao.save(catalogoModel9);
		
		return true;
	}

	
	
	
	
}
