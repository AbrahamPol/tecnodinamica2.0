package com.bolsadeideas.springboot.app.controllers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Asistencia;
import com.bolsadeideas.springboot.app.models.entity.Empleado;
import com.bolsadeideas.springboot.app.models.entity.Facturacion;
import com.bolsadeideas.springboot.app.models.service.AsistenciaService;
import com.bolsadeideas.springboot.app.models.service.EmpleadoService;

@Controller
@Secured("ROLE_USER")
@SessionAttributes("asistencia")
@RequestMapping("asistencia")
public class AsistenciaController {

	@Autowired
	EmpleadoService empleadoService;
	@Autowired
	AsistenciaService asistenciaService;

	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<Asistencia> asistencia = asistenciaService.findAllAsistencia();
		model.put("asistencia", asistencia);
		return "listar-asistencia";
	}

	///// CRUD

	// pintar formulario recibiendo id
	@RequestMapping(value = "/vista/{id}")
	public String formularioIdl(Map<String, Object> model, @PathVariable(value = "id") Long id) {
		Asistencia asistencia = asistenciaService.findAsistenciaByID(id);
		model.put("asistencia", asistencia);
		return "formulario-horario-salida";
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String actualizar(@Validated Asistencia asistencia, BindingResult result, RedirectAttributes flash,
			SessionStatus status, Map<String, Object> model) {

		if (result.hasErrors() || asistencia.getHora_salida().equals("")) {
			if (asistencia.getHora_salida().equals("")) {
				model.put("mensaje", "La hora de salida es requerida");
				return "formulario-horario-salida";
			}
			return "formulario-horario-salida";
		}

		asistenciaService.saveAsistencia(asistencia);
		flash.addFlashAttribute("success", "Actualizado con Exito!");
		status.setComplete();
		return "redirect:/asistencia";
	}

	// crear la cotizacion primera vista
	@RequestMapping(value = "/crear")
	public String crear(Map<String, Object> model) {
		// agregar titulo y objeto cotizacion
		Asistencia asistencia = new Asistencia();
		model.put("asistencia", asistencia);
		model.put("titulo", "Registro");
		// buscamos las empresas locales y las del catalogo y tambien las mandamos a
		// form
		return "formulario-asistencia";
	}

	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public String guardar(@Validated Asistencia asistencia, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash) {
		System.out.println(asistencia);

		if (result.hasErrors()) {
			return "formulario-asistencia";
		}
		asistenciaService.saveAsistencia(asistencia);
		status.setComplete();
		flash.addFlashAttribute("success", "Registrado con Exito!");
		return "redirect:/asistencia";

	}

	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(name = "id") Long id) {
		Asistencia asistencia = asistenciaService.findAsistenciaByID(id);
		asistenciaService.deleteAsistencia(asistencia);
		return "redirect:/asistencia";
	}

	/////////////////////// 7
	@GetMapping(value = "/cargar-nombre/{term}", produces = { "application/json" })
	public @ResponseBody List<Empleado> cargarEmpleados(@PathVariable String term, Map<String, Object> model) {

		return empleadoService.findByNombre(term);
	}

	@GetMapping(value = "/cargar-fecha/{term}", produces = { "application/json" })
	public @ResponseBody List<Asistencia> cargarFecha(@PathVariable String term, Map<String, Object> model) {
		return asistenciaService.findByFecha(term);
	}
	///////////////////////////////////////////////////////////////

	@RequestMapping("/especifico")
	public String especifico(Map<String, Object> model, @RequestParam(name = "fecha") String fecha) {

		List<Asistencia> asistencia = asistenciaService.findByFechaEspecifica(fecha);
		model.put("asistencia", asistencia);
		return "listar-asistencia";
	}

	///////////////////////////////////// 7

	// crear la cotizacion primera vista
	@RequestMapping(value = "/busqueda1")
	public String crear2(Map<String, Object> model) {
		// agregar titulo y objeto cotizacion
		Asistencia asistencia = new Asistencia();
		model.put("asistencia", asistencia);
		model.put("titulo", "Registro");
		// buscamos las empresas locales y las del catalogo y tambien las mandamos a
		// form
		return "formulario-busqueda-horario";
	}

	@RequestMapping(value = "/busqueda2", method = RequestMethod.POST)
	public String bus(RedirectAttributes flash, SessionStatus status, Map<String, Object> model,
			@RequestParam(name = "fecha") String fecha, @RequestParam(name = "fecha2") String fecha2,
			@RequestParam(name = "buscar_empleado") String nombre) {// buscar_empleado
		System.out.println(nombre + " Valores " + fecha);
		if (fecha.equals("") || fecha.isEmpty() || nombre.equals("") || fecha2.equals("") || fecha2.isEmpty()) {
			if (nombre.equals("")) {
				model.put("mensaje1", "No puede estar vacio");
			}
			if (fecha2.equals("")) {
				model.put("mensaje3", "Ingresa la fecha");
			}
			model.put("mensaje2", "Ingresa la fecha");
			return "formulario-busqueda-horario";
		}
		Long id = Long.parseLong(nombre);
		List<Asistencia> asistencia = asistenciaService.findByNombreFecha(fecha, fecha2, id);
		int asistencias = 0, faltas = 0, retardos = 0;

		for (Asistencia asistencia2 : asistencia) {
			// asistencia inasistencia y retardo (status)
			if (asistencia2.getEstatus().equalsIgnoreCase("asistencia")) {
				asistencias++;
			}

			if (asistencia2.getEstatus().equalsIgnoreCase("inasistencia")) {
				faltas++;
			}

			if (asistencia2.getEstatus().equalsIgnoreCase("retardo")) {
				retardos++;
			}

		}
		model.put("asistencia", asistencia);
		///
		model.put("asistencias", asistencias);
		model.put("retardos", retardos);
		model.put("faltas", faltas);
		status.setComplete();
		return "formulario-busqueda-horario";
	}

	//////////////////////////////// RANGOS///////////////////////////////////////////7
	// crear la cotizacion primera vista
	@RequestMapping(value = "/rango1")
	public String rango1(Map<String, Object> model) {
		Asistencia asistencia = new Asistencia();
		Empleado empleado= new Empleado();
		empleado.setNombre("");
		empleado.setApellido_ma("");
		empleado.setApellido_pa("");
		asistencia.setEmpleado(empleado);
		model.put("asistencia", asistencia);
//		List<Asistencia> asistencia = asistenciaService.findAllAsistencia();
//		model.put("asistencia", asistencia);
		
		model.put("titulo", "Busqueda por rangos de fecha");
		return "formulario-rango";
	}

	@RequestMapping(value = "/rango2", method = RequestMethod.POST)
	public String rango2(RedirectAttributes flash, SessionStatus status, Map<String, Object> model,
			@RequestParam(name = "fecha") String fecha, @RequestParam(name = "fecha2") String fecha2) {// buscar_empleado
		if (fecha.equals("") || fecha.isEmpty() || fecha2.equals("") || fecha2.isEmpty()) {

			if (fecha2.equals("")) {
				model.put("mensaje3", "Ingresa la fecha");
			}
			model.put("mensaje2", "Ingresa la fecha");
			return "formulario-rango";
		}

		List<Asistencia> asistencia = asistenciaService.findByrangoFecha(fecha, fecha2);
		model.put("asistencia", asistencia);
		status.setComplete();
		return "formulario-rango";
	}

}
