//package com.bolsadeideas.springboot.app.controllers;
//
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.Map;
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.SessionAttributes;
//import org.springframework.web.bind.support.SessionStatus;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import com.bolsadeideas.springboot.app.models.entity.Cliente;
//import com.bolsadeideas.springboot.app.models.service.IClienteService;
//import com.bolsadeideas.springboot.app.util.paginator.PageRender;
//
//@Controller
//@SessionAttributes("cliente")
//public class ClienteController {
//
//	@Autowired//inyeccion de dependencia de un IClienteService
//	private IClienteService clienteService;
//
//	//metodo para mostrar todos los clientes de la base de datos
//	// con su respectiva paginacion de 5 en 5 
//	@RequestMapping(value = "/listar", method = RequestMethod.GET)
//	public String listar(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {
//		//Se crea un objeto de la clase Pageable para establecer de que tamaño sera la paginacion
//		Pageable pageRequest = new PageRequest(page, 4);
//		//Se crea un tipo arraylist para guardar todos los clientes
//		Page<Cliente> clientes = clienteService.findAll(pageRequest);
//		PageRender<Cliente> pageRender = new PageRender<Cliente>("/listar", clientes);
//		model.addAttribute("titulo", "Listado de clientes");
//		model.addAttribute("clientes", clientes);
//		model.addAttribute("page", pageRender);
//		return "listar";
//	}
//
//	//el metodo crear muestra unicamente el formulario
//	@RequestMapping(value = "/form")
//	public String crear(Map<String, Object> model) {
//// se intancia un objeto de cliente y despues con el model del Map se le manda 
//		//al front end el objeto para mostrar la informacion de los clientes
//		Cliente cliente = new Cliente();
//		model.put("cliente", cliente);
//		model.put("titulo", "Crear Cliente");
//		return "form";
//	}
//
//	//aqui se muestra la interfaz del cliente para que el pueda actualizar su informacion, y la foto
//	@RequestMapping(value = "/form/{id}")
//	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {
//
//		Cliente cliente = null;
//
//		//validar que el cliente exista
//		if (id > 0) {
//			cliente = clienteService.findOne(id);
//			if (cliente == null) {
//				flash.addFlashAttribute("error", "El ID del cliente no existe en la BBDD!");
//				return "redirect:/listar";
//			}
//		} else {
//			flash.addFlashAttribute("error", "El ID del cliente no puede ser cero!");
//			return "redirect:/listar";
//		}
//		model.put("cliente", cliente);
//		model.put("titulo", "Editar Cliente");
//		return "form";
//	}
//
//	//este metodo se utiliza para hacer las peticiones del formulario
//	//editar o crear un cliente
//	//se recibe dentro de los parametros del handler una valid para confirmar que mi informacion sea correcta
//	// con respecto a lo establecido en la base de datos
//	//el BindingResult sive para encontrar los errores y asi mostrarlos
//	//el RedirectAttributes sirve para mostrar mensajes de al front end
//	//El session estatus guarda todo el objeto de cliente, se define arriba de la clase
//	// y por ultimo se recibe un MultipartFile que en este caso es la imagen
//	@RequestMapping(value = "/form", method = RequestMethod.POST)
//	public String guardar(@Valid Cliente cliente, BindingResult result, Model model, RedirectAttributes flash,
//			SessionStatus status, @RequestParam(name = "file") MultipartFile foto) {
//
//		if (result.hasErrors()) {//con el BindingResult buscamos si tenemos errores
//			model.addAttribute("titulo", "Formulario de Cliente");
//			return "form";
//		}
//		
//		if (!foto.isEmpty()) {
//			try {//codigo para guardar la imagen en una ruta externa, que es lo recomendable
//				String rootPath="C://Temp/uploads";
//				byte[] bytes=foto.getBytes();
//				Path rutacompleta=Paths.get(rootPath+"//"+foto.getOriginalFilename());
//				Files.write(rutacompleta,bytes);
//				flash.addFlashAttribute("info","Has subido correctamente '"+foto.getOriginalFilename()+"'");
//				cliente.setFoto(foto.getOriginalFilename());
//			} catch (Exception e) {
//				System.out.println("Error file"+e);
//			}
//		}
//		String mensajeFlash = (cliente.getId() != null) ? "Cliente editado con éxito!" : "Cliente creado con éxito!";
//		clienteService.save(cliente);
//		status.setComplete();
//		flash.addFlashAttribute("success", mensajeFlash);
//		return "redirect:listar";
//	}
//
//	//metodo handler para eliminar un cliente de la base de datos
//	@RequestMapping(value = "/eliminar/{id}")
//	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash) {
//
//		if (id > 0) {
//			clienteService.delete(id);
//			flash.addFlashAttribute("success", "Cliente eliminado con éxito!");
//		}
//		return "redirect:/listar";
//	}
//	
//	//metodo handler para mostrar la informacion de un cliente incluso su objeto
//	@GetMapping(value="/ver/{id}")
//	public String ver(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {
//		Cliente cliente = clienteService.findOne(id);
//		if (cliente==null) {
//			flash.addFlashAttribute("error","El cliente no existe");
//			return "redirect:/listar";
//		}
//		model.put("cliente", cliente);
//		model.put("titulo","Detalle del cliente: "+" "+cliente.getNombre());
//		return "ver";
//	}
//	
//	
//	
//}
