package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bolsadeideas.springboot.app.models.dao.CatalogoEmpresaDao;
import com.bolsadeideas.springboot.app.models.dao.EmpresaLocalDao;
import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.EmpresaLocal;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.service.CotizacionService;
import com.bolsadeideas.springboot.app.models.service.DetalleService;

@Controller
@Secured("ROLE_ADMIN")

@RequestMapping("facturas")
public class FacturasController {

	@Autowired
	CotizacionService cotizacionservice;
	@Autowired
	EmpresaLocalDao empresalocal;
	@Autowired
	CatalogoEmpresaDao empresacatalogo;

	@Autowired
	DetalleService detalleservice;
	
	// listar facturas concluidas
	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<Factura> factura=detalleservice.todos();
		model.put("factura", factura);
		return "listar-facturas";
	}

	// crear la cotizacion primera vista
	@RequestMapping(value = "/crear")
	public String crear(Map<String, Object> model) {
		// agregar titulo y objeto cotizacion
		model.put("titulo", "Crear Cotizacion");
		Cotizacion cotizacion = new Cotizacion();
		model.put("cotizacion", cotizacion);
		// buscamos las empresas locales y las del catalogo y tambien las mandamos a
		// form
		return "formulario-cotizacion";
	}

	// recibe la cotizacion para guardarla en la base de datos
//	SessionStatus status, @RequestParam(name = "file") MultipartFile foto
	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public String guardar(@Valid Cotizacion cotizacion,BindingResult result,Map<String, Object> model,
			@RequestParam(value="local") Long local,
			@RequestParam(value="catalogo") Long catalogo,Errors error) {

		if (result.hasErrors() || local==null|| catalogo==null || error.hasErrors()) {
			if (local==null) {
				model.put("local", "Recuerda ingresar el nombre de la empresa");
			}
			if(catalogo==null) {
				model.put("catalogo", "Recuerda ingresar el nombre de la empresa");
			}
			model.put("titulo", "Crear Cotizacion");
			return "formulario-cotizacion";
		}else {
			cotizacion.setEmpresa_catalogo(cotizacionservice.findCatalogoId(catalogo));
			cotizacion.setEmpresa_local(cotizacionservice.findLocalId(local));
			
		}
		try {
			cotizacionservice.guardar(cotizacion);
			cotizacionservice.guardar(cotizacion);
//			List<Cotizacion> cotizacionn = cotizacionservice.todos();
//			model.put("cotizacion", cotizacionn);
		} catch (Exception e) {
			model.put("titulo", "Crear Cotizacion");
			model.put("mensaje", "La cotizacion o referencia no se pueden repetir");
			return "formulario-cotizacion";
		}
		
		return "redirect:/concluir";

	}

	
	// buscar empresa locales
	@GetMapping(value = "/cargar-local/{term}", produces = { "application/json" })
	public @ResponseBody List<EmpresaLocal> cargarLocal(@PathVariable String term) {
		return empresalocal.findByNombre(term);
	}

	// buscar empresas de catalogo
	@GetMapping(value = "/cargar-catalogo/{term}", produces = { "application/json" })
	public @ResponseBody List<CatalogoEmpresa> cargarCatalogo(@PathVariable String term) {
		return empresacatalogo.findByNombre(term);
	}

	// buscar empresas de catalogo
	@GetMapping(value = "/cargar-facturas/{term}", produces = { "application/json" })
	public @ResponseBody List<Factura> cargarFactura(@PathVariable String term) {
		return detalleservice.todosTermino(term);
	}
}
