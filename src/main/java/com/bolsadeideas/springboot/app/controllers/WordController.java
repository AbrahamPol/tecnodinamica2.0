package com.bolsadeideas.springboot.app.controllers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;

import javax.servlet.http.Part;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.bolsadeideas.springboot.app.Word;
import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.service.DetalleService;

@Controller
@Secured("ROLE_ADMIN")

@RequestMapping("word")
public class WordController {

	@Autowired
	DetalleService detalleservice;
	private XWPFDocument document = new XWPFDocument();

	@RequestMapping(value = "/ver/{id}")
	public void cargarProductos(@PathVariable(name = "id") Long id, Map<String, Object> model) {
		Factura factura = detalleservice.factura(id);
		try {
			String doc = factura.getCotizacion().getNumero_cotizacion() + ".docx";
			// create a object of type FileOutputStream with name of the Numero_cotizacion
			// on Cotizacion
			FileOutputStream output = new FileOutputStream(doc);
			// create a new object for the document
			XWPFParagraph titulo = document.createParagraph();
			XWPFRun runTitulo = titulo.createRun();
			// Create a single Title for QA, with the object titulo
			titulo.setAlignment(ParagraphAlignment.CENTER);
			runTitulo.setBold(true);
			runTitulo.setFontSize(15);
			runTitulo.setUnderline(UnderlinePatterns.WORDS);
			runTitulo.setText("Este es el titulo");
			runTitulo.setColor("2f66f2");

			Path origenPath = FileSystems.getDefault()
					.getPath("C:\\Users\\Abraham\\git\\tecno\\spring-boot-data-jpa\\" + doc);
			Path destinoPath = FileSystems.getDefault().getPath("C:\\Temp\\uploads\\" + doc);
			try {
				Files.copy(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
				Files.delete(origenPath);
			} catch (IOException e) {
				System.err.println(e);
			}
			document.write(output);
			output.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
