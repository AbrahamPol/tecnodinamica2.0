package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bolsadeideas.springboot.app.models.dao.FacturaDao;
import com.bolsadeideas.springboot.app.models.entity.Cotizacion;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.service.CotizacionService;
import com.bolsadeideas.springboot.app.models.service.DetalleService;

@Controller
@Secured("ROLE_ADMIN")

@RequestMapping("detalle")
public class DetalleController {

	@Autowired
	DetalleService detalleservice;
	
	@RequestMapping(value = "/ver/{id}")
	public String cargarProductos(@PathVariable(name="id") Long id,Map<String, Object> model) {
		Factura factura=detalleservice.factura(id);
		model.put("factura", factura);
		return "vista-factura";

	}
	
	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(name="id") Long id) {
		Factura factura= detalleservice.factura(id);
		detalleservice.eliminarFactura(factura);
		return "redirect:/facturas";
	}

	
}
