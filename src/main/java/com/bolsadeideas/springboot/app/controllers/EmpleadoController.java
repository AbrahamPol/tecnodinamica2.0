package com.bolsadeideas.springboot.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.dao.EmpleadoDao;
import com.bolsadeideas.springboot.app.models.entity.CatalogoEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Empleado;
import com.bolsadeideas.springboot.app.models.entity.Facturacion;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.service.EmpleadoService;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

@Controller
@Secured("ROLE_USER")
@SessionAttributes("empleado")
@RequestMapping("empleados")
public class EmpleadoController {

	@Autowired
	EmpleadoService empleadoService;

	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<Empleado> empleado = empleadoService.findAllEmpleados();
		model.put("empleado", empleado);
		return "listar-empleado";
	}
	///// CRUD

	// pintar formulario recibiendo id
	@RequestMapping(value = "/vista/{id}")
	public String formularioIdl(Map<String, Object> model, @PathVariable(value = "id") Long id) {
		Empleado empleado = empleadoService.findEmpleadoByID(id);
		model.put("empleado", empleado);
		return "vista-empleado";
	}



	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String actualizar(@Validated Empleado empleado, BindingResult result, RedirectAttributes flash,
			SessionStatus status, @RequestParam(name = "file") MultipartFile foto) {

		if (result.hasErrors()) {
			return "vista-empleado";
		}

//		if (!foto.isEmpty()) {
//			try {// codigo para guardar la imagen en una ruta externa, que es lo recomendable
//				String rootPath = "C://Temp/uploads";
//				byte[] bytes = foto.getBytes();
//				Path rutacompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
//				Files.write(rutacompleta, bytes);
//				flash.addFlashAttribute("info", "Has subido correctamente '" + foto.getOriginalFilename() + "'");
//				empleado.setFoto(foto.getOriginalFilename());
//			} catch (Exception e) {
//				System.out.println("Error file" + e);
//			}
//		}

		if (!foto.isEmpty()) {

			String uniqueFilename = UUID.randomUUID().toString() + "_" + foto.getOriginalFilename();
			Path rootPath = Paths.get("uploads").resolve(uniqueFilename);
			Path rootAbsolutPath = rootPath.toAbsolutePath();
			try {
				Files.copy(foto.getInputStream(), rootAbsolutPath);
				flash.addFlashAttribute("info", "Has subido correctamente '" + uniqueFilename + "'");
				empleado.setFoto(uniqueFilename);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		empleado.setStatus(true);
		empleadoService.saveEmpleado(empleado);
		flash.addFlashAttribute("success", "Empleado actualizado con Exito!");
		status.setComplete();
		return "redirect:/empleados";
	}

	// crear la cotizacion primera vista
	@RequestMapping(value = "/crear")
	public String crear(Map<String, Object> model) {
		// agregar titulo y objeto cotizacion
		Empleado empleado = new Empleado();
		model.put("empleado", empleado);
		model.put("titulo", "Nuevo empleado");
		// buscamos las empresas locales y las del catalogo y tambien las mandamos a
		// form
		return "formulario-empleado";
	}

	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public String guardar(@Validated Empleado empleado, BindingResult result, Map<String, Object> model,
			SessionStatus status, RedirectAttributes flash, @RequestParam(name = "file") MultipartFile foto) {

		if (result.hasErrors() || foto.isEmpty()) {
			if (result.hasErrors()) {
				model.put("titulo", "Nuevo empleado");
				model.put("mensaje", "No puede estar vacio");
			}
			if (foto.isEmpty()) {
				model.put("titulo", "Nueva empleado");
				model.put("mensaje2", "No puede estar vacio");
			}
			return "formulario-empleado";
		}

//		if (!foto.isEmpty()) {
//			try {// codigo para guardar la imagen en una ruta externa, que es lo recomendable
//				String rootPath = "C://Temp/uploads";
//				byte[] bytes = foto.getBytes();
//				Path rutacompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
//				Files.write(rutacompleta, bytes);
//				flash.addFlashAttribute("info", "Has subido correctamente '" + foto.getOriginalFilename() + "'");
//				empleado.setFoto(foto.getOriginalFilename());
//			} catch (Exception e) {
//				System.out.println("Error file" + e);
//			}
//		}

		if (!foto.isEmpty()) {

			String uniqueFilename = UUID.randomUUID().toString() + "_" + foto.getOriginalFilename();
			Path rootPath = Paths.get("uploads").resolve(uniqueFilename);
			Path rootAbsolutPath = rootPath.toAbsolutePath();
			try {
				Files.copy(foto.getInputStream(), rootAbsolutPath);
				flash.addFlashAttribute("info", "Has subido correctamente '" + uniqueFilename + "'");
				empleado.setFoto(uniqueFilename);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		empleado.setStatus(true);
		empleadoService.saveEmpleado(empleado);
		status.setComplete();
		flash.addFlashAttribute("success", "Empleado guardado con Exito!");
		return "redirect:/empleados";
	}

	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(name = "id") Long id) {
		Empleado empleado = empleadoService.findEmpleadoByID(id);
		empleado.setStatus(false);
		empleadoService.saveEmpleado(empleado);
		return "redirect:/empleados";
	}

////find by name
	@GetMapping(value = "/cargar-nombre/{term}", produces = { "application/json" })
	public @ResponseBody List<Empleado> cargarEmpleados(@PathVariable String term, Map<String, Object> model) {

		return empleadoService.findByNombre(term);
	}

}// restcontroller
