package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.service.DetalleService;

@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("")
public class InicioController {

	@Autowired
	DetalleService detalleservice;

	@RequestMapping("")
	public String principal(Map<String, Object> model) {
		List<Factura> factura = detalleservice.todos();
		model.put("factura", factura);
		return "listar-facturas";
	}

}
